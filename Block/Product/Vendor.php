<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */

namespace Elogic\Vendor\Block\Product;

use Elogic\Vendor\Api\Data\VendorInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Block\Product\AwareInterface as ProductAwareInterface;
use Magento\Catalog\Block\Product\Context as ProductContext;
use Magento\Catalog\Model\Product;
use Magento\Framework\Api\AttributeInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Elogic\Vendor\Api\VendorRepositoryInterface as VendorRepository;

/**
 * Class Vendor
 */
class Vendor extends Template implements ProductAwareInterface
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var VendorRepository
     */
    protected $vendorRepository;

    /**
     * Vendor constructor.
     *
     * @param ProductContext $context
     * @param array $data
     */
    public function __construct(
        ProductContext $context,
        VendorRepository $vendorRepository,
        array $data = []
    ) {
        $this->coreRegistry = $context->getRegistry();
        $this->vendorRepository = $vendorRepository;
        parent::__construct($context, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function setProduct(ProductInterface $product)
    {
        $this->setData('product', $product);
    }

    /**
     * {@inheritdoc}
     */
    public function getProduct()
    {
        if (!$this->hasData('product')) {
            $this->setData('product', $this->coreRegistry->registry('product'));
        }
        return $this->getData('product');
    }

    /**
     * Get Vendor list
     *
     * @return []
     */
    public function getVendorList()
    {
        $list = [];
        /** @var Product $product */
        $product = $this->getProduct();
        /** @var AttributeInterface $attribute */
        $attribute = $product->getCustomAttribute(VendorInterface::PRODUCT_ATTR_VENDOR);
        if ($attribute) {
            $vendorIds = trim($attribute->getValue(), ',');
            $vendorIds = explode(',', $vendorIds);
            foreach ($vendorIds as $vendorId) {
                /** @var VendorInterface $vendor */
                $vendor = $this->vendorRepository->load($vendorId);
                $list[] = $vendor->getName();
            }
        }

        return $list;
    }
}