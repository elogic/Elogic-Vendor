<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */

namespace Elogic\Vendor\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

use Elogic\Vendor\Api\Data\VendorInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $this->createVendorTable($setup);

        $installer->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     *
     * @return void
     */
    private function createVendorTable(SchemaSetupInterface $setup)
    {
        $table = $setup->getConnection()
            ->newTable(
                $setup->getTable(VendorSchema::TABLE_VENDOR)
            );

        $table
            ->addColumn(
                VendorInterface::VENDOR_ID,
                Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'identity' => true
                ],
                'Vendor id'
            )
            ->addColumn(
                VendorInterface::VENDOR_NAME,
                Table::TYPE_TEXT,
                255,
                [
                    'nullable' => false
                ],
                'Vendor name'
            );

        $setup->getConnection()->createTable($table);
    }
}
