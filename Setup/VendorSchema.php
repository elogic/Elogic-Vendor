<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */

namespace Elogic\Vendor\Setup;

/**
 * Interface VendorSchema
 */
interface VendorSchema
{
    const TABLE_VENDOR = 'test_vendor';
}
