<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */

namespace Elogic\Vendor\Setup;

use Elogic\Vendor\Api\Data\VendorInterface;
use Elogic\Vendor\Api\Data\VendorInterfaceFactory;
use Elogic\Vendor\Api\VendorRepositoryInterface as VendorRepository;
use Elogic\Vendor\Model\Product\Attribute\Source\Vendor as AttributeSourceVendor;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class InstallData
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var VendorRepository
     */
    protected $vendorRepository;

    /**
     * @var VendorInterfaceFactory
     */
    protected $vendorFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        VendorRepository $vendorRepository,
        VendorInterfaceFactory $vendorFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->vendorRepository = $vendorRepository;
        $this->vendorFactory = $vendorFactory;
    }

    /**
     * Install
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        /**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            Product::ENTITY,
            VendorInterface::PRODUCT_ATTR_VENDOR,
            [
                'type' => 'varchar',
                'backend' => 'Elogic\Vendor\Model\Product\Attribute\Backend\Vendor',
                'frontend' => '',
                'label' => 'Vendor',
                'input' => 'multiselect',//TODO: implement multiselect
                'class' => '',
                'source' => AttributeSourceVendor::class,
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing'=> true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'General',
            ]
        );

        /**
         * Install Sample Data
         */
        $sampleData = [
            'Acer',
            'Dell',
            'Lenovo',
            'Apple',
            'Samsung',
        ];

        foreach ($sampleData as $vendorName) {
            /** @var VendorInterface $vendor */
            $vendor = $this->vendorFactory->create();
            $vendor->setName($vendorName);
            $this->vendorRepository->save($vendor);
        }

        $installer->endSetup();
    }
}
