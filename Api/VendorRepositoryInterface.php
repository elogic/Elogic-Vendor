<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */
namespace Elogic\Vendor\Api;

use Elogic\Vendor\Api\Data\VendorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;

/**
 * Interface VendorRepositoryInterface
 */
interface VendorRepositoryInterface
{
    /**
     * Load by Id
     *
     * @param int $id
     *
     * @return AbstractModel
     * @throws NoSuchEntityException
     */
    public function load($id);

    /**
     * Save
     *
     * @param AbstractModel $vendor
     *
     * @return $this
     * @throws CouldNotSaveException
     */
    public function save(AbstractModel $vendor);

    /**
     * @param SearchCriteriaInterface|null $searchCriteria
     *
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null);

    /**
     * @param VendorInterface $vendor
     */
    public function getAssociatedProductIds(VendorInterface $vendor);
}