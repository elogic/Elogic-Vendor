<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */
namespace Elogic\Vendor\Api\Data;

/**
 * Interface VendorInterface
 */
interface VendorInterface
{
    const VENDOR_ID = 'vendor_id';
    const VENDOR_NAME = 'name';
    const PRODUCT_ATTR_VENDOR = 'vendor';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     */
    public function setId($id);

    /**
     * @param int $entityId
     */
    public function setEntityId($entityId);

    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param string $name
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();
}