<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */

namespace Elogic\Vendor\Model\Product\Attribute\Backend;

use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Elogic\Vendor\Api\Data\VendorInterface;
use Magento\Framework\DataObject;


class Vendor extends AbstractBackend
{
    /**
     * Prepare data for save
     *
     * @param DataObject $object
     * @return AbstractBackend
     */
    public function beforeSave($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $data = $object->getData($attributeCode);
        if (is_array($data)) {
            $data = array_filter($data);
            $object->setData($attributeCode, ',' . implode(',', $data) . ',');
        }

        return parent::beforeSave($object);
    }

    /**
     * Implode data for validation
     *
     * @param DataObject $object
     * @return bool
     */
    public function validate($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $data = $object->getData($attributeCode);
        if (is_array($data)) {
            $object->setData($attributeCode, implode(',', array_filter($data)));
        }
        return parent::validate($object);
    }
}