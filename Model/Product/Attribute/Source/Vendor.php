<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */

namespace Elogic\Vendor\Model\Product\Attribute\Source;

use Elogic\Vendor\Api\Data\VendorInterface;
use Elogic\Vendor\Api\VendorRepositoryInterface as VendorRepository;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Api\SearchResultsInterface;

/**
 * Class Vendor
 */
class Vendor extends AbstractSource
{
    /**
     * @var VendorRepository
     */
    protected $vendorRepository;

    /**
     * Vendor constructor.
     *
     * @param VendorRepository $vendorRepository
     */
    public function __construct(
        VendorRepository $vendorRepository
    ) {
        $this->vendorRepository = $vendorRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllOptions()
    {
        if (empty($this->_options)) {
            $options = [];
            /** @var SearchResultsInterface $searchResults */
            $searchResults = $this->vendorRepository->getList();
            /** @var VendorInterface $vendor */
            foreach ($searchResults->getItems() as $vendor) {
                $options[] = [
                    'value' => $vendor->getId(),
                    'label' => __($vendor->getName()),
                ];
            }
            $this->_options = $options;
        }

        return $this->_options;
    }
}