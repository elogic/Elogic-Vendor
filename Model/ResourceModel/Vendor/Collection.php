<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */

namespace Elogic\Vendor\Model\ResourceModel\Vendor;

use Elogic\Vendor\Model\ResourceModel\Vendor as VendorResource;
use Elogic\Vendor\Model\Vendor;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 */
class Collection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(Vendor::class, VendorResource::class);
    }
}