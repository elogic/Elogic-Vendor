<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */

namespace Elogic\Vendor\Model\ResourceModel;

use Elogic\Vendor\Api\Data\VendorInterface;
use Elogic\Vendor\Setup\VendorSchema;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Vendor
 */
class Vendor extends AbstractDb
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(VendorSchema::TABLE_VENDOR, VendorInterface::VENDOR_ID);
    }
}