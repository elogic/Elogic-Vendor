<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */

namespace Elogic\Vendor\Model;

use Elogic\Vendor\Api\Data\VendorInterface;
use Elogic\Vendor\Model\ResourceModel\Vendor as VendorResource;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Vendor
 */
class Vendor extends AbstractModel implements VendorInterface
{
    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_idFieldName = VendorInterface::VENDOR_ID;

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(VendorResource::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getEntityId()
    {
        return $this->_getData(VendorInterface::VENDOR_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setEntityId($entityId)
    {
        $this->setData(VendorInterface::VENDOR_ID, $entityId);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->_getData(VendorInterface::VENDOR_NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->setData(VendorInterface::VENDOR_NAME, $name);
    }
}