<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */

namespace Elogic\Vendor\Repository;

use Elogic\Vendor\Api\Data\VendorInterface;
use Elogic\Vendor\Api\Data\VendorInterfaceFactory;
use Elogic\Vendor\Api\VendorRepositoryInterface;
use Elogic\Vendor\Model\ResourceModel\Vendor as VendorResourceModel;
use Elogic\Vendor\Model\ResourceModel\Vendor\Collection as VendorCollection;
use Elogic\Vendor\Model\Vendor;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Api\SearchResultsFactory;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\AbstractModel;

/**
 * Class VendorRepository
 */
class VendorRepository implements VendorRepositoryInterface
{
    /**
     * Cache key for Associated Product Ids
     *
     * @var string
     */
    protected $keyAssociatedProductIds = '_cache_instance_associated_product_ids';

    /**
     * @var SearchResultsFactory
     */
    protected $searchResultsFactory;

    /**
     * @var VendorInterfaceFactory
     */
    protected $vendorFactory;

    /**
     * @var VendorResourceModel
     */
    protected $vendorResourceModel;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * VendorRepository constructor.
     *
     * @param SearchResultsFactory $searchResultsFactory
     * @param VendorInterfaceFactory $vendorFactory
     * @param VendorResourceModel $vendorResourceModel
     * @param ProductCollectionFactory $productCollectionFactory
     */
    public function __construct(
        SearchResultsFactory $searchResultsFactory,
        VendorInterfaceFactory $vendorFactory,
        VendorResourceModel $vendorResourceModel,
        ProductCollectionFactory $productCollectionFactory
    ) {
        $this->searchResultsFactory = $searchResultsFactory;
        $this->vendorFactory = $vendorFactory;
        $this->vendorResourceModel = $vendorResourceModel;
        $this->productCollectionFactory = $productCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(AbstractModel $vendor)
    {
        try {
            $this->vendorResourceModel->save($vendor);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $vendor;
    }

    /**
     * {@inheritdoc}
     */
    public function load($id)
    {
        return $this->getById($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null)
    {
        /** @var Vendor $vendorModel */
        $vendorModel = $this->vendorFactory->create();
        /** @var VendorCollection $collection */
        $collection = $vendorModel->getCollection();
        /** @var SearchResults $searchResults */
        $searchResults = $this->searchResultsFactory->create();

        if (!is_null($searchCriteria)) {
            // Add filters from root filter group to the collection
            /** @var FilterGroup $group */
            foreach ($searchCriteria->getFilterGroups() as $group) {
                $this->addFilterGroupToCollection($group, $collection);
            }

            $sortOrders = $searchCriteria->getSortOrders();
            if ($sortOrders) {
                /** @var SortOrder $sortOrder */
                foreach ($sortOrders as $sortOrder) {
                    $field = $sortOrder->getField();
                    $collection->addOrder(
                        $field,
                        ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                    );
                }
            }
            $collection->setCurPage($searchCriteria->getCurrentPage());
            $collection->setPageSize($searchCriteria->getPageSize());

            $searchResults->setSearchCriteria($searchCriteria);
        }

        $collection->load();

        $vendors = [];
        /** @var VendorInterface $vendor */
        foreach ($collection->getItems() as $vendor) {
            /**
             * TODO: just for test
             */
//            $this->getAssociatedProductIds($vendor);

            $vendors[] = $this->getById($vendor->getId());
        }
        $searchResults->setItems($vendors);
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * Retrieve vendor.
     *
     * @param int $vendorId
     *
     * @return VendorInterface
     * @throws NoSuchEntityException
     */
    public function getById($vendorId)
    {
        /** @var Vendor $vendor */
        $vendor = $this->vendorFactory->create();
        $this->vendorResourceModel->load($vendor, $vendorId);
        if (!$vendor->getId()) {
            throw new NoSuchEntityException(__('Vendor with id "%1" does not exist.', $vendorId));
        }
        return $vendor;
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param VendorCollection $collection
     *
     * @return void
     * @throws InputException
     */
    protected function addFilterGroupToCollection(FilterGroup $filterGroup, VendorCollection $collection)
    {
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAssociatedProductIds(VendorInterface $vendor)
    {
        /** @var Vendor $vendor  */
        if (!$vendor->hasData($this->keyAssociatedProductIds)) {
            /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
            $collection = $this->productCollectionFactory->create();
            $collection->addAttributeToSelect(VendorInterface::PRODUCT_ATTR_VENDOR);
            $collection->addFieldToFilter(VendorInterface::PRODUCT_ATTR_VENDOR, ['like' => '%,' . $vendor->getId() . ',%']);

            $associatedProductIds = $collection->getAllIds();
            $vendor->setData($this->keyAssociatedProductIds, $associatedProductIds);
        }
        return $vendor->getData($this->keyAssociatedProductIds);
    }
}