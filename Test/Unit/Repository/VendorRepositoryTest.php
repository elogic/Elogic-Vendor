<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */

namespace Elogic\Vendor\Test\Unit\Repository;

use Elogic\Vendor\Test\Unit\AbstractTestCase;

use Elogic\Vendor\Repository\VendorRepository;

use Elogic\Vendor\Api\Data\VendorInterface;
use Elogic\Vendor\Api\Data\VendorInterfaceFactory;
use Elogic\Vendor\Api\VendorRepositoryInterface;
use Elogic\Vendor\Model\ResourceModel\Vendor as VendorResourceModel;
use Elogic\Vendor\Model\ResourceModel\Vendor\Collection as VendorCollection;
use Elogic\Vendor\Model\Vendor;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Api\SearchResultsFactory;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\AbstractModel;

/**
 * Class VendorRepositoryTest
 */
class VendorRepositoryTest extends AbstractTestCase
{
    /**
     * @var VendorRepository
     */
    protected $vendorRepository;
    /**
     * @var SearchResultsFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $searchResultsFactoryMock;

    /**
     * @var VendorInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $vendorFactoryMock;

    /**
     * @var VendorResourceModel|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $vendorResourceModelMock;

    /**
     * @var ProductCollectionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $productCollectionFactoryMock;

    /**
     * @var AbstractModel|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $abstractModelMock;

    /**
     * NoSuchEntityException
     *
     * @var NoSuchEntityException|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $errorMock;

    /**
     * Setup
     *
     * @return  void
     */
    public function setUp()
    {
        parent::setUp();

        $this->searchResultsFactoryMock = $this->basicMock(SearchResultsFactory::class);
        $this->vendorFactoryMock = $this->basicMock(VendorInterfaceFactory::class);
        $this->vendorResourceModelMock = $this->basicMock(VendorResourceModel::class);
        $this->productCollectionFactoryMock = $this->basicMock(ProductCollectionFactory::class);

        $this->vendorRepository = $this->objectManager->getObject(
            VendorRepository::class,
            [
                'searchResultsFactory' => $this->searchResultsFactoryMock,
                'vendorFactory' => $this->vendorFactoryMock,
                'vendoResourceModel' => $this->vendorResourceModelMock,
                'productCollectionFactory' => $this->productCollectionFactoryMock,
            ]
        );
        $this->abstractModelMock = $this->basicMock(AbstractModel::class);
        $this->errorMock = $this->basicMock(NoSuchEntityException::class);
        

    }

    /**
     * @dataProvider saveDataProvider
     */
    public function testSave($exception)
    {
        if ($exception) {
            $this->vendorResourceModelMock->expects($this->any())
                ->willThrowException($this->errorMock);
            $this->vendorRepository->save($this->abstractModelMock);
            return;
        }
        $this->assertEquals($this->abstractModelMock, $this->vendorRepository->save($this->abstractModelMock));
    }

    /**
     * @return array
     */
    public function saveDataProvider()
    {
        return [
            [0],
            [1],
        ];
    }
}