<?php
/**
 * @copyright Copyright (c) 2014-2017 Elogic (http://www.elogic.com.ua)
 *
 * @see PROJECT_LICENSE.txt
 */

namespace Elogic\Vendor\Test\Unit;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class AbstractTestCase
 */
abstract class AbstractTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * Object Manager Instance
     *
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager object Manager
     */
    protected $objectManager;

    /**
     * SetUp
     *
     * @return void
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);
    }

    /**
     * Build a basic (stub methods only) mock object
     *
     * @param string $className
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function basicMock($className)
    {
        return $this->getMockBuilder($className)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * Build a full (mock methods only) mock object
     *
     * @param string $className
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function fullMock($className)
    {
        return $this->getMockBuilder($className)
            ->disableOriginalConstructor()
            ->setMethods(null)
            ->getMock();
    }

    /**
     * Build a mixed (stub and mock methods) mock object
     *
     * @param string $className
     * @param array $methods
     * @param null|array $constructorArgs
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function mixedMock($className, array $methods, $constructorArgs = null)
    {
        if ($constructorArgs === null) {

            return $this->getMockBuilder($className)
                ->disableOriginalConstructor()
                ->setMethods($methods)
                ->getMock();
        } else {

            return $this->getMockBuilder($className)
                ->setConstructorArgs($constructorArgs)
                ->setMethods($methods)
                ->getMock();
        }
    }

    /**
     * Provides Boolean Data Array
     *
     * @return array
     */
    public function booleanDataProvider()
    {
        return [[true], [false]];
    }
    
    /**
     * Call protected/private method of a class.
     *
     * @param object &$object Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
